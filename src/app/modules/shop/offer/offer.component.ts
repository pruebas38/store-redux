import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  @Input() offers: any;
  @Output() itemSelected = new EventEmitter;

  public formOffer: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.formOffer = this.formBuilder.group({
      data: [null, []],
    });
  }

  ngOnInit(): void {
    console.log({offers: this.offers});
  }

  setOfferData(): void {
    const f: FormGroup = this.formOffer;
    const id = f.get('data')?.value;

    let itemData = this.offers.filter((data: any) => data.versions[0].id == id);

    this.itemSelected.emit(itemData[0]);
  }

}
