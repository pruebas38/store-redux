import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  public offers: any;
  public item: any;
  public prices: any
  public characteristics: any;

  constructor(
    private dataService: DataService,
  ) {}

  ngOnInit(): void {
    this.getOffers();
  }

  private getOffers(): void {
    this.dataService.getOffers()
    .subscribe(response => {
      this.offers = response;
    });

  }

  public itemSelected(offer: any): void {
    this.prices = offer.versions[0].productOfferingPrices;
    this.characteristics = offer.versions[0].characteristics;
    this.item = offer;
    console.log(offer.versions[0].characteristics);
  }

}
