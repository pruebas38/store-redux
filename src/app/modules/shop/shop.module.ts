import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { StoreRoutingModule } from './shop-routing.module';
import { OfferComponent } from './offer/offer.component';
import { CharacteristicsComponent } from './characteristics/characteristics.component';
import { PricesComponent } from './prices/prices.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PriceComponent } from './prices/price/price.component';
import { CharacteristicComponent } from './characteristics/characteristic/characteristic.component';



@NgModule({
  declarations: [
    ShopComponent,
    OfferComponent,
    CharacteristicsComponent,
    PricesComponent,
    PriceComponent,
    CharacteristicComponent
  ],
  imports: [
    CommonModule,
    StoreRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ShopModule { }
